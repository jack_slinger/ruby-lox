require 'spec_helper'

RSpec.describe RubyLox::Parser do

  describe "#parse" do

    context "When parsing a simple valid expression" do
      specify "Then it returns the correct syntax tree" do
        tokens = RubyLox::Scanner.new(source: "1 + 2 == 4;", error_reporter: nil).scan_tokens
        ast = described_class.new(tokens: tokens, error_reporter: nil).parse.first
        expect(RubyLox::AstPrinter.new.print(ast)).to eq "( expression_statement ( == ( + 1.0 2.0 ) 4.0 ) )"
      end
    end

  end

end