require 'spec_helper'

RSpec.describe RubyLox::Lox do
  let(:error_reporter) { double(:error_reporter, had_error: false, had_runtime_error: false) }
  let(:output) { double(:output) }
  subject { described_class.new(error_reporter: error_reporter, output: output) }

  specify "Then it can store and print variables" do
    expect(output).to receive(:puts).with("Apple")
    subject.send(:run, "var a = \"Apple\"; print a;")
  end

  specify "Then it can assign new values to variables" do
    expect(output).to receive(:puts).with("New Apple")
    subject.send(:run, "var a = \"Apple\"; a = \"New Apple\"; print a;")
  end

end