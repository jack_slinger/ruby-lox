require 'spec_helper'

RSpec.describe RubyLox::AstPrinter do

  describe '#print' do
    specify 'It can print a string literal' do
      literal = RubyLox::Expression::Literal.new(value: 'hello')
      grouping = RubyLox::Expression::Grouping.new(expression: literal)
      expect(subject.print(grouping)).to eq "( group hello )"
    end

    specify 'It can print a binary expression with two literals' do
      one = RubyLox::Expression::Literal.new(value: 1)
      less = RubyLox::Token.new(type: :LESS, lexeme: "<", line: 1)
      two = RubyLox::Expression::Literal.new(value: 2)
      binary = RubyLox::Expression::Binary.new(left: one, operator: less, right: two)
      expect(subject.print(binary)).to eq "( < 1 2 )"
    end

    specify 'It can print a unary expression' do
      tokens = RubyLox::Scanner.new(source: "!!true;", error_reporter: nil).scan_tokens
      ast = RubyLox::Parser.new(tokens: tokens, error_reporter: nil).parse.first
      expect(subject.print(ast)).to eq "( expression_statement ( ! ( ! true ) ) )"
    end
  end

end