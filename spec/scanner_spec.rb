require 'spec_helper'

RSpec.describe RubyLox::Scanner do

  describe '#scan_tokens' do
    subject(:scanner) { described_class.new(source: source, error_reporter: error_reporter) }
    let(:error_reporter) { double(:error_reporter) }
    let(:tokens) { scanner.scan_tokens }
    let(:token_types) { tokens.map(&:type) }

    context 'When given an empty string' do
      let(:source) { '' }

      specify 'Then it returns the EOF token' do
        tokens = scanner.scan_tokens
        expect(token_types).to eq [:EOF]
      end
    end

    # Single character tokens
    context 'When given (' do
      let(:source) { '(' }

      specify 'Then it returns the LEFT_PAREN token followed by the EOF token' do
        expect(token_types).to eq [:LEFT_PAREN, :EOF]
      end
    end

    context 'When given )' do
      let(:source) { ')' }

      specify 'Then it returns the RIGHT_PAREN token followed by the EOF token' do
        expect(token_types).to eq [:RIGHT_PAREN, :EOF]
      end
    end

    context 'When given {' do
      let(:source) { '{' }

      specify 'Then it returns the LEFT_BRACE token followed by the EOF token' do
        expect(token_types).to eq [:LEFT_BRACE, :EOF]
      end
    end
    
    context 'When given }' do
      let(:source) { '}' }

      specify 'Then it returns the RIGHT_BRACE token followed by the EOF token' do
        expect(token_types).to eq [:RIGHT_BRACE, :EOF]
      end
    end
    
    context 'When given ,' do
      let(:source) { ',' }

      specify 'Then it returns the COMMA token followed by the EOF token' do
        expect(token_types).to eq [:COMMA, :EOF]
      end
    end
    
    context 'When given .' do
      let(:source) { '.' }

      specify 'Then it returns the DOT token followed by the EOF token' do
        expect(token_types).to eq [:DOT, :EOF]
      end
    end
    
    context 'When given -' do
      let(:source) { '-' }

      specify 'Then it returns the MINUS token followed by the EOF token' do
        expect(token_types).to eq [:MINUS, :EOF]
      end
    end
    
    context 'When given +' do
      let(:source) { '+' }

      specify 'Then it returns the PLUS token followed by the EOF token' do
        expect(token_types).to eq [:PLUS, :EOF]
      end
    end
    
    context 'When given ;' do
      let(:source) { ';' }

      specify 'Then it returns the SEMICOLON token followed by the EOF token' do
        expect(token_types).to eq [:SEMICOLON, :EOF]
      end
    end
    
    context 'When given *' do
      let(:source) { '*' }

      specify 'Then it returns the STAR token followed by the EOF token' do
        expect(token_types).to eq [:STAR, :EOF]
      end
    end
    
    context 'When given !' do
      let(:source) { '!' }

      specify 'Then it returns the BANG token followed by the EOF token' do
        expect(token_types).to eq [:BANG, :EOF]
      end
    end

    context 'When given =' do
      let(:source) { '=' }

      specify 'Then it returns the EQUAL token followed by the EOF token' do
        expect(token_types).to eq [:EQUAL, :EOF]
      end
    end

    context 'When given <' do
      let(:source) { '<' }

      specify 'Then it returns the LESS token followed by the EOF token' do
        expect(token_types).to eq [:LESS, :EOF]
      end
    end

    context 'When given >' do
      let(:source) { '>' }

      specify 'Then it returns the GREATER token followed by the EOF token' do
        expect(token_types).to eq [:GREATER, :EOF]
      end
    end

    context 'When given /' do
      let(:source) { '/' }

      specify 'Then it returns the SLASH token followed by the EOF token' do
        expect(token_types).to eq [:SLASH, :EOF]
      end
    end

    context 'When given a space character' do
      let(:source) { ' ' }

      specify 'Then it ignores it' do
        expect(token_types).to eq [:EOF]
      end
    end

    context 'When given a tab' do
      let(:source) { "\t" }

      specify 'Then it ignores it' do
        expect(token_types).to eq [:EOF]
      end
    end

    context 'When given a carrage return' do
      let(:source) { "\r" }

      specify 'Then it ignores it' do
        expect(token_types).to eq [:EOF]
      end
    end

    context 'When given a new line' do
      let(:source) { "=\n/" }

      specify 'Then it increments the line counter for the next lot of tokens' do
        expect(tokens.map{|token| [token.type, token.line]}).to eq [[:EQUAL, 1], [:SLASH, 2], [:EOF, 2]]
      end
    end

    # Multicharacter tokens
    context 'When given !=' do
      let(:source) { '!=' }

      specify 'Then it returns the BANG_EQUAL token followed by the EOF token' do
        expect(token_types).to eq [:BANG_EQUAL, :EOF]
      end
    end

    context 'When given !!=!=!' do
      let(:source) { '!!=!=!' }

      specify 'Then it returns the correct sequence of tokens' do
        expect(token_types).to eq [:BANG, :BANG_EQUAL, :BANG_EQUAL, :BANG, :EOF]
      end
    end

    context 'When given ==' do
      let(:source) { '==' }

      specify 'Then it returns the EQUAL_EQUAL token followed by the EOF token' do
        expect(token_types).to eq [:EQUAL_EQUAL, :EOF]
      end
    end

    context 'When given =!======' do
      let(:source) { '=!======' }

      specify 'Then it returns the correct sequence of tokens' do
        expect(token_types).to eq [:EQUAL, :BANG_EQUAL, :EQUAL_EQUAL, :EQUAL_EQUAL, :EQUAL, :EOF]
      end
    end

    context 'When given <=' do
      let(:source) { '<=' }

      specify 'Then it returns the LESS_EQUAL token followed by the EOF token' do
        expect(token_types).to eq [:LESS_EQUAL, :EOF]
      end
    end

    context 'When given >=' do
      let(:source) { '>=' }

      specify 'Then it returns the GREATER_EQUAL token followed by the EOF token' do
        expect(token_types).to eq [:GREATER_EQUAL, :EOF]
      end
    end

    context 'When given == // this is a comment, it can contain anything.\n=' do
      let(:source) { "== // this is a comment, it can contain anything\n=" }

      specify 'Then it ignores the comment which is from the // until a new line or end of file.' do
        expect(token_types).to eq [:EQUAL_EQUAL, :EQUAL, :EOF]
      end
    end

    # Literals
    context 'When given a string literal on a single line' do
      let(:source) { '"here is a string."' }

      specify 'Then it correctly tokenizes the string literal' do
        expect(token_types).to eq [:STRING, :EOF]
        expect(tokens.first.literal).to eq 'here is a string.'
      end
    end

    context 'When given a string literal containing newlines' do
      let(:source) { "\"Here is line one.\nHere is line two\"" }

      specify 'Then it correctly tokenizes the string literal' do
        expect(token_types).to eq [:STRING, :EOF]
        expect(tokens.first.line).to eq 2
      end
    end

    context 'When given an unterminated string literal' do
      let(:source) { '"Here is a cool string without a closing quote' }

      specify 'Then it returns an error' do
        expect(error_reporter).to receive(:scanner_error).with(line: 1, message: 'Unterminated string.')
        scanner.scan_tokens
      end
    end

    context 'When given an integer literal' do
      let(:source) { '123' }

      specify 'Then it correctly tokenizes the number' do
        expect(token_types).to eq [:NUMBER, :EOF]
        expect(tokens.first.literal).to eq 123
      end
    end

    context 'When given a decimal literal' do
      let(:source) { '123.456' }

      specify 'Then it correctly tokenizes the number' do
        expect(token_types).to eq [:NUMBER, :EOF]
        expect(tokens.first.literal).to eq 123.456
      end
    end

    context 'When given a number that starts with a .' do
      let(:source) { '.342' }

      specify 'Then it returns a dot token followed by a number token' do
        expect(token_types).to eq [:DOT, :NUMBER, :EOF]
        expect(tokens[1].literal).to eq 342
      end
    end

    context 'When given a number that ends with a .' do
      let(:source) { '893.' }

      specify 'Then it returns the number token followed by a dot token' do
        expect(token_types).to eq [:NUMBER, :DOT, :EOF]
        expect(tokens.first.literal).to eq 893
      end
    end

    context 'When given a number that starts with a 0' do
      let(:source) { '012' }

      specify 'Then it tokenizes the number as a decimal NOT as octal' do
        expect(token_types).to eq [:NUMBER, :EOF]
        expect(tokens.first.literal).to eq 12
      end
    end

    # Keywords and identifiers
    context 'When given the keyword "and"' do
      let(:source) { 'and' }

      specify 'Then it returns AND followed by EOF' do
        expect(token_types).to eq [:AND, :EOF]
      end
    end

    context 'When given the keyword "class"' do
      let(:source) { 'class' }

      specify 'Then it returns CLASS followed by EOF' do
        expect(token_types).to eq [:CLASS, :EOF]
      end
    end
    
    context 'When given the keyword "else"' do
      let(:source) { 'else' }

      specify 'Then it returns ELSE followed by EOF' do
        expect(token_types).to eq [:ELSE, :EOF]
      end
    end
    
    context 'When given the keyword "false"' do
      let(:source) { 'false' }

      specify 'Then it returns FALSE followed by EOF' do
        expect(token_types).to eq [:FALSE, :EOF]
      end
    end
    
    context 'When given the keyword "for"' do
      let(:source) { 'for' }

      specify 'Then it returns FOR followed by EOF' do
        expect(token_types).to eq [:FOR, :EOF]
      end
    end
    
    context 'When given the keyword "fun"' do
      let(:source) { 'fun' }

      specify 'Then it returns FUN followed by EOF' do
        expect(token_types).to eq [:FUN, :EOF]
      end
    end
    
    context 'When given the keyword "if"' do
      let(:source) { 'if' }

      specify 'Then it returns IF followed by EOF' do
        expect(token_types).to eq [:IF, :EOF]
      end
    end
    
    context 'When given the keyword "nil"' do
      let(:source) { 'nil' }

      specify 'Then it returns NIL followed by EOF' do
        expect(token_types).to eq [:NIL, :EOF]
      end
    end
    
    context 'When given the keyword "or"' do
      let(:source) { 'or' }

      specify 'Then it returns OR followed by EOF' do
        expect(token_types).to eq [:OR, :EOF]
      end
    end
    
    context 'When given the keyword "print"' do
      let(:source) { 'print' }

      specify 'Then it returns PRINT followed by EOF' do
        expect(token_types).to eq [:PRINT, :EOF]
      end
    end
    
    context 'When given the keyword "return"' do
      let(:source) { 'return' }

      specify 'Then it returns RETURN followed by EOF' do
        expect(token_types).to eq [:RETURN, :EOF]
      end
    end
    
    context 'When given the keyword "super"' do
      let(:source) { 'super' }

      specify 'Then it returns SUPER followed by EOF' do
        expect(token_types).to eq [:SUPER, :EOF]
      end
    end
    
    context 'When given the keyword "this"' do
      let(:source) { 'this' }

      specify 'Then it returns THIS followed by EOF' do
        expect(token_types).to eq [:THIS, :EOF]
      end
    end
    
    context 'When given the keyword "true"' do
      let(:source) { 'true' }

      specify 'Then it returns TRUE followed by EOF' do
        expect(token_types).to eq [:TRUE, :EOF]
      end
    end
    
    context 'When given the keyword "var"' do
      let(:source) { 'var' }

      specify 'Then it returns VAR followed by EOF' do
        expect(token_types).to eq [:VAR, :EOF]
      end
    end
    
    context 'When given the keyword "while"' do
      let(:source) { 'while' }

      specify 'Then it returns WHILE followed by EOF' do
        expect(token_types).to eq [:WHILE, :EOF]
      end
    end

    context 'When given a non keyword identifer' do
      let(:source) { 'myCoolVariable2' }

      specify 'Then it returns IDENTIFIER, followed by EOF' do
        expect(token_types).to eq [:IDENTIFIER, :EOF]
      end
    end

    # Classes
    

    # Invalid Characters
    context 'When given some invalid characters' do
      let(:source) { '@#^' }

      specify 'Then it records an error for each invalid character' do
        expect(error_reporter).to receive(:scanner_error).with(line: 1, message: 'Unexpected character: @')
        expect(error_reporter).to receive(:scanner_error).with(line: 1, message: 'Unexpected character: #')
        expect(error_reporter).to receive(:scanner_error).with(line: 1, message: 'Unexpected character: ^')
        scanner.scan_tokens
      end
    end

    # Full examples
    context 'When given an example from the book' do
      let(:source) { "// this is a comment\n(( )){} // grouping stuff\n!*+-/=<> <= == // operators" }

      specify 'Then it produces the correct sequence of tokens' do
        expect(token_types).to eq [
          :LEFT_PAREN,:LEFT_PAREN, :RIGHT_PAREN, :RIGHT_PAREN, :LEFT_BRACE, :RIGHT_BRACE,
          :BANG, :STAR, :PLUS, :MINUS, :SLASH, :EQUAL, :LESS, :GREATER, :LESS_EQUAL, :EQUAL_EQUAL, :EOF
        ]
      end
    end

    context 'When given a simple example' do
      let(:source) { "i = 1;\nwhile i < 10;\n{ i = i + 1; print i; }" }

      specify 'Then it produces the correct sequence of tokens' do
        expect(token_types).to eq [
          :IDENTIFIER, :EQUAL, :NUMBER, :SEMICOLON, :WHILE, :IDENTIFIER, :LESS, :NUMBER, :SEMICOLON,
          :LEFT_BRACE, :IDENTIFIER, :EQUAL, :IDENTIFIER, :PLUS, :NUMBER, :SEMICOLON,
          :PRINT, :IDENTIFIER, :SEMICOLON, :RIGHT_BRACE, :EOF
        ]
      end
    end

  end
end