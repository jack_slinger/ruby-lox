require "zeitwerk"
loader = Zeitwerk::Loader.for_gem
loader.setup
loader.eager_load

module RubyLox
  class Lox
    attr_reader :error_reporter

    def initialize(error_reporter: ConsoleErrorReporter.new, environment: Environment.new, output: $stdout)
      @error_reporter = error_reporter
      @environment = environment
      @output = output
    end

    def run_prompt
      puts 'LOX REPL:'
      puts '\q to quit'

      while true
        print 'lox> '
        $stdout.flush
        input = STDIN.gets
        exit if input == "\\q\n"
        run(input)
        error_reporter.reset_had_error
      end
    end

    def run_file(filepath)
      input = File.read(filepath)
      run(input)

      exit(65) if had_error
      exit(70) if had_error
      exit
    end

    def had_error
      error_reporter.had_error
    end

    def had_runtime_error
      error_reporter.had_runtime_error
    end

    private

    def run(input)
      scanner = Scanner.new(source: input, error_reporter: @error_reporter)
      tokens = scanner.scan_tokens

      parser = Parser.new(tokens: tokens, error_reporter: @error_reporter)
      statements = parser.parse()

      return if had_error

      interpreter = Interpreter.new(error_reporter: @error_reporter, environment: @environment, output: @output)
      
      resolver = Resolver.new(interpreter, error_reporter: @error_reporter)
      
      resolver.resolve(statements)
      return if had_runtime_error

      interpreter.interpret(statements)
    end
  end
end
