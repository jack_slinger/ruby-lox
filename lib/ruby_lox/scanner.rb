require_relative './token.rb'

module RubyLox
  class Scanner
    attr_reader :source, :error_reporter, :tokens

    def initialize(source:, error_reporter:)
      @source = source
      @error_reporter = error_reporter
      @tokens = []
      @start = 0
      @current = 0
      @line = 1
    end

    def scan_tokens
      @tokens = []
      @start = 0
      @current = 0
      @line = 1

      while !is_at_end
        scan_token
      end

      @tokens << Token.new(type: :EOF, lexeme: "", line: @line)
      tokens
    end

    private

    def scan_token
      @start = @current
      character = advance
      case character
      when '(' then add_token(:LEFT_PAREN)
      when ')' then add_token(:RIGHT_PAREN)
      when '{' then add_token(:LEFT_BRACE)
      when '}' then add_token(:RIGHT_BRACE)
      when ',' then add_token(:COMMA)
      when '.' then add_token(:DOT)
      when '-' then add_token(:MINUS)
      when '+' then add_token(:PLUS)
      when ';' then add_token(:SEMICOLON)
      when '*' then add_token(:STAR)
      when '!' then match('=') ? add_token(:BANG_EQUAL) : add_token(:BANG)
      when '=' then match('=') ? add_token(:EQUAL_EQUAL) : add_token(:EQUAL)
      when '<' then match('=') ? add_token(:LESS_EQUAL) : add_token(:LESS)
      when '>' then match('=') ? add_token(:GREATER_EQUAL) : add_token(:GREATER)
      when '/'
        if match('/')
          advance while !is_at_end && !match("\n")
        else
          add_token(:SLASH)
        end
      when '"' then tokenize_string
      when /[0-9]/ then tokenize_number
      when /[a-zA-Z_]/ then tokenize_keyword_or_identifier
      when ' ' then nil
      when "\t" then nil
      when "\r" then nil
      when "\n" then (@line += 1)
      else
        error_reporter.scanner_error(line: @line, message: "Unexpected character: #{character}")
      end
    end

    def is_at_end
      @current >= source.size
    end

    def add_token(type, literal=nil)
      tokens << Token.new(type: type, lexeme: source[@start...@current], line: @line, literal: literal)
    end

    def advance
      @current += 1
      source[@current - 1]
    end

    def peek
      source[@current]
    end

    def peek_next
      source[@current+1]
    end

    def match(character_to_check)
      return false if is_at_end
      if peek == character_to_check
        advance
        return true
      else
        return false
      end
    end

    def tokenize_string
      while !is_at_end && peek != '"'
        @line += 1 if peek == "\n"
        advance
      end

      if is_at_end
        error_reporter.scanner_error(line: @line, message: 'Unterminated string.')
      else
        # Consume the closing quote "
        advance
        # The literal is the string without the opening and closing "
        literal = source[@start + 1...@current - 1]
        add_token(:STRING, literal)
      end
    end

    def tokenize_number
      advance while !is_at_end && peek =~ /[0-9]/

      if peek == '.' && peek_next =~ /[0-9]/
        advance # Consume the '.'
        advance while !is_at_end && peek =~ /[0-9]/
      end

      literal = source[@start...@current].to_f
      add_token(:NUMBER, literal)
    end

    def tokenize_keyword_or_identifier
      advance while !is_at_end && peek =~ /[_a-zA-Z0-9]/
      text = source[@start...@current]
      case text
      when 'and' then add_token(:AND)
      when 'class' then add_token(:CLASS)
      when 'else' then add_token(:ELSE)
      when 'true' then add_token(:TRUE)
      when 'false' then add_token(:FALSE)
      when 'for' then add_token(:FOR)
      when 'fun' then add_token(:FUN)
      when 'if' then add_token(:IF)
      when 'nil' then add_token(:NIL)
      when 'or' then add_token(:OR)
      when 'print' then add_token(:PRINT)
      when 'return' then add_token(:RETURN)
      when 'super' then add_token(:SUPER)
      when 'this' then add_token(:THIS)
      when 'var' then add_token(:VAR)
      when 'while' then add_token(:WHILE)
      else
        add_token(:IDENTIFIER)
      end
    end

  end
end