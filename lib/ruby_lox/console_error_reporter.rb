module RubyLox
  class ConsoleErrorReporter
    attr_reader :had_error, :had_runtime_error

    def initialize
      @had_error = false
      @had_runtime_error = false
    end

    def report(line:, where:, message:)
      puts "[line #{line}] Error#{where}: #{message}"
    end

    def scanner_error(line:, message:)
      @had_error = true
      report(line: line, where: "", message: message)
    end

    def parser_error(token:, message:)
      @had_error = true
      if token.type == :EOF
        report(line: token.line, where: " at end", message: message)
      else
        report(line: token.line, where: " at '#{token.lexeme}'", message: message)
      end
    end

    def runtime_error(error)
      @had_runtime_error = true
      puts "#{error.message}\n[line #{error.token.line}]"
    end

    def reset_had_error
      @had_error = false
    end

  end
end