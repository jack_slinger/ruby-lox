module RubyLox
  class Token
    attr_reader :type, :lexeme, :literal, :line

    def initialize(type:, lexeme:, line:, literal: nil)
      @type = type
      @lexeme = lexeme
      @line = line
      @literal = literal
    end

    def to_s
      "#{type} #{lexeme} #{literal}"
    end

  end
end