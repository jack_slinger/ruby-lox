module RubyLox
  class LoxClass < LoxCallable

    attr_reader :name, :methods

    def initialize(name, methods)
      @name = name
      @methods = methods
    end

    def call(interpreter, arguments)
      instance = LoxInstance.new(self)
      initializer = find_method("init")
      unless initializer.nil?
        initializer.bind(instance).call(interpreter, arguments)
      end
      return instance
    end

    def arity
      initializer = find_method("init")
      if initializer.nil?
        0
      else
        initializer.arity()
      end
    end

    def find_method(lexeme)
      if methods.key?(lexeme)
        return methods[lexeme]
      end
    end

    def to_s
      name
    end

  end
end