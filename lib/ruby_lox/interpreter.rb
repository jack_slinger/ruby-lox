module RubyLox
  class Interpreter
    attr_reader :error_reporter, :environment, :globals, :locals

    def initialize(error_reporter:, environment:, output: $stdout)
      @error_reporter = error_reporter
      @globals = environment
      @locals = {}
      @environment = @globals
      @output = output

      @globals.define("clock", NativeFunctions::Clock.new)
    end

    def interpret(statements)
      begin
        statements.each do |statement|
          execute(statement)
        end
      rescue LoxRuntimeError => error
        error_reporter.runtime_error(error)
      end
    end

    def resolve(expression, depth)
      locals[expression] = depth
    end

    def visit_binary_expr(expression)
      left = evaluate(expression.left)
      right = evaluate(expression.right)
      operator = expression.operator

      case expression.operator.type
      when :BANG_EQUAL
        !isEqual(left, right)
      when :EQUAL
        isEqual(left, right)
      when :GREATER
        checkNumberOperands!(operator, left, right)
        left > right
      when :GREATER_EQUAL
        checkNumberOperands!(operator, left, right)
        left >= right
      when :LESS
        checkNumberOperands!(operator, left, right)
        left < right
      when :LESS_EQUAL
        checkNumberOperands!(operator, left, right)
        left <= right
      when :MINUS
        checkNumberOperands!(operator, left, right)
        left - right
      when :PLUS
        checkNumberOrStringOperands!(operator, left, right)
        left + right
      when :SLASH
        checkNumberOperands!(operator, left, right)
        left / right
      when :STAR
        checkNumberOperands!(operator, left, right)
        left * right
      end
    end

    def visit_call_expr(expression)
      callee = evaluate(expression.callee)

      arguments = expression.arguments.map do |argument|
        evaluate(argument)
      end

      unless callee.is_a?(LoxCallable)
        raise LoxRuntimeError.new(expression.paren, "Can only call functions and classes.")
      end
      if arguments.size != callee.arity
        raise LoxRuntimeError.new(expression.paren, "Expected #{callee.arity} arguments but got #{arguments.size}.")
      end
      return callee.call(self, arguments)
    end

    def visit_get_expr(expression)
      object = evaluate(expression.object)
      if object.is_a?(LoxInstance)
        return object.get(expression.name)
      else
        raise LoxRuntimeError.new(expression.name, "Only instances have properties.")
      end
    end

    def visit_set_expr(expression)
      object = evaluate(expression.object)

      unless object.is_a?(LoxInstance)
        raise LoxRuntimeError.new(expression.name, "Only instances have fields.")
      end

      value = evaluate(expression.value)
      object.set(expression.name, value)
      return value
    end

    def visit_this_expr(expression)
      lookup_variable(expression.keyword, expression)
    end

    def visit_logical_expr(expression)
      left = evaluate(expression.left)

      if expression.operator.type == :OR
        return left if is_truthy(left)
      else
        return left if !is_truthy(left)
      end

      return evaluate(expression.right)
    end

    def visit_grouping_expr(expression)
      evaluate(expression.expression)
    end

    def visit_literal_expr(expression)
      expression.value
    end

    def visit_unary_expr(expression)
      right = evaluate(expression.right)

      case expression.operator.type
      when :BANG
        !is_truthy(right)
      when :MINUS
        checkNumberOperand!(expression.operator, right)
        -right
      end
    end

    def visit_variable_expr(expression)
      lookup_variable(expression.name, expression)
    end

    def visit_assignment_expr(expression)
      value = evaluate(expression.value)

      distance = locals[expression]
      if distance.nil?
        globals.assign(expression.name, value)
      else
        environment.assign_at(distance, expression.name, value)
      end

      return value
    end

    def visit_expression_stmt(statement)
      evaluate(statement.expression)
      return nil
    end

    def visit_function_stmt(statement)
      function = LoxFunction.new(declaration: statement, closure: environment)
      environment.define(statement.name.lexeme, function)
      return nil
    end

    def visit_class_stmt(statement)
      environment.define(statement.name.lexeme, nil)

      methods = statement.methods.map do |method|
        [method.name.lexeme, LoxFunction.new(declaration: method, closure: environment, is_initializer: method.name.lexeme == "init")]
      end.to_h

      klass = LoxClass.new(statement.name.lexeme, methods)
      environment.assign(statement.name, klass)
      return nil
    end

    def visit_print_stmt(statement)
      value = evaluate(statement.expression)
      @output.puts stringify(value)
      return nil
    end

    def visit_return_stmt(statement)
      value = statement.value.nil? ? nil : evaluate(statement.value)
      raise Return.new(value)
    end

    def visit_while_stmt(statement)
      while is_truthy(evaluate(statement.condition))
        execute(statement.body)
      end
      return nil
    end

    def visit_variable_stmt(statement)
      value = statement.initializer.nil? ? nil : evaluate(statement.initializer)
      environment.define(statement.name.lexeme, value)
      return nil
    end

    def visit_block_stmt(statement)
      execute_block(statement.statements, Environment.new(enclosing: environment))
      return nil
    end

    def visit_if_stmt(statement)
      if is_truthy(evaluate(statement.condition))
        execute(statement.then_branch)
      elsif !statement.else_branch.nil?
        execute(statement.else_branch)
      end
      return nil
    end

    private

    def execute(statement)
      statement.accept(self)
    end

    def execute_block(statements, new_environment)
      previous = environment
      begin
        @environment = new_environment

        statements.each do |statement|
          execute(statement)
        end
      ensure
        @environment = previous
      end
    end

    def evaluate(expression)
      expression.accept(self)
    end

    def is_truthy(object)
      # false and nil are falsey
      # everything else is truthy
      !!object
    end

    def isEqual(a, b)
      a == b
    end

    def stringify(object)
      object.to_s

      # TODO: Handle displaying numbers correctly
    end

    def checkNumberOperand!(operator, operand)
      unless operand.is_a?(Float)
        raise LoxRuntimeError.new(operator, "Operand must be a number.")
      end
    end

    def checkNumberOperands!(operator, left, right)
      unless left.is_a?(Float) && right.is_a?(Float)
        raise LoxRuntimeError.new(operator, "Operands must be a numbers.")
      end
    end

    def checkNumberOrStringOperands!(operator, left, right)
      unless (left.is_a?(Float) && right.is_a?(Float)) || (left.is_a?(String) && right.is_a?(String))
        raise LoxRuntimeError.new(operator, "Operands must be two numbers or two strings.")
      end
    end

    def lookup_variable(name, expression)
      distance = locals[expression]
      if distance.nil?
        globals.get(name)
      else
        environment.get_at(distance, name.lexeme)
      end
    end
  end
end