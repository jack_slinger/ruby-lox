module RubyLox
  module NativeFunctions
    class Clock < LoxCallable

      def arity
        0
      end

      def call(interpreter, arguments)
        Time.now.to_i
      end

      def to_s
        "<native fn>"
      end

    end
  end
end