module RubyLox
  module Expression
    class Unary
      attr_reader :operator, :right

      def initialize(operator:, right:)
        @operator = operator
        @right = right
      end

      def accept(visitor)
        visitor.visit_unary_expr(self)
      end
    end
  end
end