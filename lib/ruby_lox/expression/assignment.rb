module RubyLox
  module Expression
    class Assignment
      attr_reader :name, :value

      def initialize(name:, value:)
        @name = name
        @value = value
      end

      def accept(visitor)
        visitor.visit_assignment_expr(self)
      end
    end
  end
end