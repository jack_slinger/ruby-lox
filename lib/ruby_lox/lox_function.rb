module RubyLox
  class LoxFunction < LoxCallable
    attr_reader :declaration, :closure, :is_initializer

    def initialize(declaration:, closure:, is_initializer: false)
      @declaration = declaration
      @closure = closure
      @is_initializer = is_initializer
    end

    def arity
      declaration.parameters.size
    end

    def call(interpreter, arguments)
      environment = Environment.new(enclosing: closure)
      declaration.parameters.each_with_index do |parameter, index|
        environment.define(parameter.lexeme, arguments[index])
      end

      begin
        interpreter.send(:execute_block, declaration.body, environment)
      rescue Return => return_value
        if is_initializer
          return closure.get_at(0, "this")
        else
          return return_value.value
        end
      end

      return closure.get_at(0, "this") if is_initializer
      
      return nil
    end

    def bind(instance)
      environment = Environment.new(enclosing: closure)
      environment.define("this", instance)
      return LoxFunction.new(declaration: declaration, closure: environment, is_initializer: is_initializer)
    end

    def to_s
      "<fn #{declaration.name.lexeme}>"
    end
  end
end