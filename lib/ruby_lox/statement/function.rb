module RubyLox
  module Statement
    class Function
      attr_reader :name, :parameters, :body

      def initialize(name:, parameters:, body:)
        @name = name
        @parameters = parameters
        @body = body
      end

      def accept(visitor)
        visitor.visit_function_stmt(self)
      end
    end
  end
end