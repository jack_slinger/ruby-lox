module RubyLox
  module Statement
    class Variable
      attr_reader :name, :initializer

      def initialize(name:, initializer:)
        @name = name
        @initializer = initializer
      end

      def accept(visitor)
        visitor.visit_variable_stmt(self)
      end
    end
  end
end