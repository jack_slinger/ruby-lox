module RubyLox
  module Statement
    class Return
      attr_reader :keyword, :value

      def initialize(keyword:, value:)
        @keyword = keyword
        @value = value
      end

      def accept(visitor)
        visitor.visit_return_stmt(self)
      end
    end
  end
end