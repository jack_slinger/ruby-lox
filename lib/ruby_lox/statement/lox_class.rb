module RubyLox
  module Statement
    class LoxClass
      attr_reader :name, :methods

      def initialize(name:, methods:)
        @name = name
        @methods = methods
      end

      def accept(visitor)
        visitor.visit_class_stmt(self)
      end
    end
  end
end