module RubyLox
  module Statement
    class Print
      attr_reader :expression

      def initialize(expression:)
        @expression = expression
      end

      def accept(visitor)
        visitor.visit_print_stmt(self)
      end
    end
  end
end