module RubyLox
  module Statement
    class While
      attr_reader :condition, :body

      def initialize(condition:, body:)
        @condition = condition
        @body = body
      end

      def accept(visitor)
        visitor.visit_while_stmt(self)
      end
    end
  end
end