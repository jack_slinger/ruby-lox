module RubyLox
  class LoxInstance

    attr_reader :klass, :fields

    def initialize(klass)
      @klass = klass
      @fields = {}
    end

    def get(name)
      if fields.key?(name.lexeme)
        return fields[name.lexeme]
      end
      
      method = klass.find_method(name.lexeme)
      return method.bind(self) unless method.nil?
    
      raise LoxRuntimeError.new(name, "Undefined property '#{name.lexeme}'.")
    end

    def set(name, value)
      fields[name.lexeme] = value
    end

    def to_s
      "#{klass.name} instance"
    end

  end
end