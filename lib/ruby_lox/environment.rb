module RubyLox
  class Environment

    attr_reader :values, :enclosing

    def initialize(enclosing: nil)
      @values = {}
      @enclosing = enclosing
    end

    def define(name, value)
      @values[name] = value
    end

    def assign(name, value)
      if name_defined?(name)
        @values[name.lexeme] = value
      elsif !@enclosing.nil?
        @enclosing.assign(name, value)
      else
        raise LoxRuntimeError.new(name, "Undefined variable '#{name.lexeme}'.")
      end

      return nil
    end

    def assign_at(distance, name, value)
      ancestor(distance).values[name.lexeme] = value
    end

    def get(name)
      if name_defined?(name)
        @values[name.lexeme]
      elsif !@enclosing.nil?
        @enclosing.get(name)
      else
        raise LoxRuntimeError.new(name, "Undefined variable '#{name.lexeme}'.")
      end
    end

    def get_at(distance, lexeme)
      ancestor(distance).values[lexeme]
    end

    private

    def name_defined?(name)
      @values.keys.include?(name.lexeme)
    end

    def ancestor(distance)
      environment = self
      distance.times do
        environment = environment.enclosing
      end
      return environment
    end
  end
end