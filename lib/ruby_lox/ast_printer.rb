module RubyLox
  class AstPrinter

    def print(expression)
      expression.accept(self)
    end

    def visit_binary_expr(expression)
      parenthesize expression.operator.lexeme, expression.left, expression.right
    end

    def visit_grouping_expr(expression)
      parenthesize 'group', expression.expression
    end

    def visit_literal_expr(expression)
      expression.value.nil? ? 'nil' : expression.value
    end

    def visit_unary_expr(expression)
      parenthesize expression.operator.lexeme, expression.right
    end

    def visit_expression_stmt(statement)
      parenthesize 'expression_statement', statement.expression
    end

    private

    def parenthesize(name, *expressions)
      value = "( #{name} "
      value += expressions.map { |expression| expression.accept(self).to_s }.join(" ")
      value += " )"
      return value
    end

  end
end