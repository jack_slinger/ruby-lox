module RubyLox
  class Resolver
    attr_accessor :interpreter, :scopes, :current_function, :current_class

    def initialize(interpreter, error_reporter:)
      @interpreter = interpreter
      @error_reporter = error_reporter
      @scopes = []
      @current_function = :none
      @current_class = :none
    end

    def resolve(statements)
      begin
        statements.each do |statement|
          statement.accept(self)
        end
      rescue LoxRuntimeError => error
        @error_reporter.runtime_error(error)
      end
    end

    def visit_block_stmt(statement)
      begin_scope
      resolve(statement.statements)
      end_scope
      return nil
    end

    def visit_variable_stmt(statement)
      declare(statement.name)
      unless statement.initializer.nil?
        statement.initializer.accept(self)
      end
      define(statement.name)
      return nil
    end

    def visit_variable_expr(expression)
      if !scopes.empty? && scopes.last[expression.name.lexeme] == false
        # TODO: Which error class should we return here?
        raise "#{expression.name} Can't read local variable in its own initializer."
      end
      resolve_local(expression, expression.name)
      return nil
    end

    def visit_assignment_expr(expression)
      expression.value.accept(self)
      resolve_local(expression, expression.name)
      return nil
    end

    def visit_function_stmt(statement)
      declare(statement.name)
      define(statement.name)

      resolve_function(statement, :function)
      return nil
    end

    def visit_class_stmt(statement)
      enclosing_class = current_class
      @current_class = :class

      declare(statement.name)
      define(statement.name)

      begin_scope()
      scopes.last["this"] = true

      statement.methods.each do |method|
        declaration = method.name.lexeme == "init" ? :initializer : :method
        resolve_function(method,  declaration)
      end

      end_scope()

      current_class = enclosing_class
      return nil
    end

    def visit_expression_stmt(statement)
      statement.expression.accept(self)
      return nil
    end

    def visit_if_stmt(statement)
      statement.condition.accept(self)
      statement.then_branch.accept(self)
      statement.else_branch.accept(self) unless statement.else_branch.nil?
      return nil
    end

    def visit_print_stmt(statement)
      statement.expression.accept(self)
      return nil
    end

    def visit_return_stmt(statement)
      if current_function == :none
        raise LoxRuntimeError.new(statement.keyword, "Can't return from top-level code.")
      end

      unless statement.value.nil?
        if current_function == :initializer
          raise LoxRuntimeError.new(statement.keyword, "Can't return a value from an initializer.")
        end
        statement.value.accept(self)
      end
      return nil
    end

    def visit_while_stmt(statement)
      statement.condition.accept(self)
      statement.body.accept(self)
      return nil
    end

    def visit_binary_expr(expression)
      expression.left.accept(self)
      expression.right.accept(self)
      return nil
    end

    def visit_call_expr(expression)
      expression.callee.accept(self)
      expression.arguments.each do |argument|
        argument.accept(self)
      end
      return nil
    end

    def visit_get_expr(expression)
      expression.object.accept(self)
      return nil
    end

    def visit_set_expr(expression)
      expression.value.accept(self)
      expression.object.accept(self)
      return nil
    end

    def visit_this_expr(expression)
      if current_class == :none
        # TODO: The book makes this a different kind of error.
        raise LoxRuntimeError.new(expression.keyword, "Can't use 'this' outside of a class.")
      end

      resolve_local(expression, expression.keyword)
      return nil
    end

    def visit_grouping_expr(expression)
      expression.expression.accept(self)
      return nil
    end

    def visit_literal_expr(expression)
    end

    def visit_logical_expr(expression)
      expression.left.accept(self)
      expression.right.accept(self)
      return nil
    end

    def visit_unary_expr(expression)
      expression.right.accept(self)
      return nil
    end

    private

    def begin_scope
      @scopes.push({})
    end

    def end_scope
      @scopes.pop
    end

    def declare(name)
      return if scopes.empty?
      scope = scopes.last
      if scope.key?(name.lexeme)
        raise LoxRuntimeError.new(name, "Already a variable with this name in this scope.")
      end
      scope[name.lexeme] = false
    end

    def define(name)
      return if scopes.empty?
      scope = scopes.last
      scope[name.lexeme] = true
    end

    def resolve_local(expression, name)
      scopes.reverse.each_with_index do |scope, i|
        if scope.key?(name.lexeme)
          interpreter.resolve(expression, i)
          return
        end
      end
    end

    def resolve_function(function, type)
      enclosing_function = current_function
      current_function = type

      begin_scope
      function.parameters.each do |param|
        declare(param)
        define(param)
      end
      resolve(function.body)
      end_scope
      current_function = enclosing_function
    end
  end
end