module RubyLox
  class Parser

    def initialize(tokens:, error_reporter:)
      @tokens = tokens
      @error_reporter = error_reporter
      @current = 0
    end

    def parse
      statements = []
      while !isAtEnd
        statements << declaration()
      end
      
      statements
    end

    private

    def declaration
      begin
        return class_declaration() if match(:CLASS)
        return function("function") if match(:FUN)
        return var_declaration() if match(:VAR)
        statement()
      rescue ParseError => ex
        synchronize()
        return nil
      end
    end

    def class_declaration
      name = consume(:IDENTIFIER, "Expect class name.")
      consume(:LEFT_BRACE, "Expect '{' before class body.")

      methods = []
      while(!check(:RIGHT_BRACE) && !isAtEnd)
        methods << function("method")
      end

      consume(:RIGHT_BRACE, "Expect '}' after class body.")
      return Statement::LoxClass.new(name: name, methods: methods)
    end

    def statement
      return for_statement() if match(:FOR)
      return if_statement() if match(:IF)
      return print_statement() if match(:PRINT)
      return return_statement() if match(:RETURN)
      return while_statement() if match(:WHILE)
      return Statement::Block.new(statements: block()) if match(:LEFT_BRACE)
      expression_statement()
    end

    def block
      statements = []
      while !check(:RIGHT_BRACE) && !isAtEnd
        statements << declaration()
      end

      consume(:RIGHT_BRACE, "Expect '}' after block.")
      return statements
    end

    def var_declaration
      name = consume(:IDENTIFIER, "Expect variable name.")
      initializer = match(:EQUAL) ? expression() : nil
      consume(:SEMICOLON, "Expect ';' after variable declaration.")
      Statement::Variable.new(name: name, initializer: initializer)
    end

    def for_statement
      consume(:LEFT_PAREN, "Expect '(' after 'for'.")

      initializer = if match(:SEMICOLON)
        nil
      elsif match(:VAR)
        var_declaration()
      else
        expression_statement()
      end

      condition = check(:SEMICOLON) ? Expression::Literal(true) : expression()
      consume(:SEMICOLON, "Expect ';' after loop condition.")

      increment = check(:RIGHT_PAREN) ? nil : expression()
      consume(:RIGHT_PAREN, "Expect ')' after for clauses.")

      body = statement()

      unless increment.nil?
        # Add the increment expression after the body
        body = Statement::Block.new(statements: [body, Statement::Expression.new(expression: increment)])
      end

      # Add the while loop with the condition
      body = Statement::While.new(condition: condition, body: body)

      unless initializer.nil?
        # Add the initializer before the while loop
        body = Statement::Block.new(statements: [initializer, body])
      end

      return body
    end

    def if_statement
      consume(:LEFT_PAREN, "Expect '(' after 'if'.")
      condition = expression()
      consume(:RIGHT_PAREN, "Expect ')' after if condition.")

      then_branch = statement()
      else_branch = match(:ELSE) ? statement() : nil
      Statement::If.new(condition: condition, then_branch: then_branch, else_branch: else_branch)
    end

    def print_statement
      expr = expression()
      consume(:SEMICOLON, "Expect ';' after value.")
      Statement::Print.new(expression: expr)
    end

    def return_statement
      keyword = previous()
      value = check(:SEMICOLON) ? nil : expression()
      consume(:SEMICOLON, "Expect ';' after return value.")
      return Statement::Return.new(keyword: keyword, value: value)
    end

    def while_statement
      consume(:LEFT_PAREN, "Expect '(' after 'while'.")
      condition = expression()
      consume(:RIGHT_PAREN, "Expect ')' after condition.")
      body = statement()

      return Statement::While.new(condition: condition, body: body)
    end

    def expression_statement
      expr = expression()
      consume(:SEMICOLON, "Expect ';' after expression.")
      Statement::Expression.new(expression: expr)
    end

    def function(kind)
      name = consume(:IDENTIFIER, "Expect #{kind} name.")
      consume(:LEFT_PAREN, "Expect '(' after #{kind} name.")

      parameters = []
      unless check(:RIGHT_PAREN)
        loop do
          if parameters.size >= 255
            error(peek(), "Can't have more than 255 parameters.")
          end
          parameters << consume(:IDENTIFIER, "Expect parameter name.")
          break unless match(:COMMA)
        end
      end
      consume(:RIGHT_PAREN, "Expect ')' after parameters.")

      consume(:LEFT_BRACE, "Expect '{' before #{kind} body.")
      body = block()
      return Statement::Function.new(name: name, parameters: parameters, body: body)
    end

    
    def expression
      assignment()
    end

    def assignment
      expr = or_method()

      if match(:EQUAL)
        equals = previous()
        value = assignment()

        if expr.is_a?(Expression::Variable)
          name = expr.name
          return Expression::Assignment.new(name: name, value: value)
        elsif expr.is_a?(Expression::Get)
          return Expression::Set.new(object: expr.object, name: expr.name, value: value)
        end

        error(equals, "Invalid assignment target.")
      end

      return expr
    end

    def or_method
      expr = and_method()

      while match(:OR)
        operator = previous()
        right = and_method()
        expr = Expression::Logical.new(left: expr, operator: operator, right: right)
      end

      return expr
    end

    def and_method
      expr = equality()

      while match(:AND)
        operator = previous()
        right = equality()
        expr Expression::Logical.new(left: expr, operator: operator, right: right)
      end

      return expr
    end

    def equality
      expr = comparison()
      while match(:BANG_EQUAL, :EQUAL_EQUAL) do
        operator = previous()
        right = comparison()
        expr = Expression::Binary.new(left: expr, operator: operator, right: right)
      end
      return expr
    end

    def comparison
      expr = term()
      while match(:GREATER, :GREATER_EQUAL, :LESS, :LESS_EQUAL) do
        operator = previous()
        right = term()
        expr = Expression::Binary.new(left: expr, operator: operator, right: right)
      end
      return expr
    end

    def term
      expr = factor()
      while match(:MINUS, :PLUS) do
        operator = previous()
        right = factor()
        expr = Expression::Binary.new(left: expr, operator: operator, right: right)
      end
      return expr
    end
    
    def factor
      expr = unary()
      while match(:SLASH, :STAR) do
        operator = previous()
        right = unary()
        expr = Expression::Binary.new(left: expr, operator: operator, right: right)
      end
      return expr
    end

    def unary
      if match(:BANG, :MINUS)
        operator = previous()
        right = unary()
        Expression::Unary.new(operator: operator, right: right)
      else
        call()
      end
    end

    def call
      expr = primary()

      while true
        if match(:LEFT_PAREN)
          expr = finish_call(expr)
        elsif match(:DOT)
          name = consume(:IDENTIFIER, "Expect property name after '.'.")
          expr = Expression::Get.new(object: expr, name: name)
        else
          break
        end
      end

      return expr
    end

    def primary
      return Expression::Literal.new(value: false) if match(:FALSE)
      return Expression::Literal.new(value: true) if match(:TRUE)
      return Expression::Literal.new(value: nil) if match(:NIL)

      return Expression::Literal.new(value: previous().literal) if match(:NUMBER, :STRING)

      return Expression::This.new(keyword: previous()) if match(:THIS)

      return Expression::Variable.new(name: previous()) if match(:IDENTIFIER)

      if match(:LEFT_PAREN)
        expr = expression()
        consume(:RIGHT_PAREN, "Expect ')' after expression.")
        return Expression::Grouping.new(expression: expr)
      end

      raise error(peek(), "Expect expression.")
    end

    def match(*types)
      types.each do |type|
        if check(type)
          advance()
          return true
        end
      end
      false
    end

    def check(type)
      return false if isAtEnd
      peek().type == type
    end

    def advance
      @current +=1 unless isAtEnd
      previous()
    end

    def isAtEnd
      peek().type == :EOF
    end

    def peek
      @tokens[@current]
    end

    def previous
      @tokens[@current - 1]
    end

    def consume(type, message)
      return advance() if check(type)
      raise error(peek(), message)
    end

    def error(token, message)
      @error_reporter.parser_error(token: token, message: message)
      ParseError.new()
    end

    def synchronize
      advance()
      while !isAtEnd do
        previous_token = previous()
        return if previous_token.type == :SEMICOLON
        
        likely_start_of_statement = [:CLASS, :FOR, :FUN, :IF, :PRINT, :RETURN, :VAR, :WHILE]
        return if likely_start_of_statement.include?(peek().type)
        advance()
      end
    end

    def finish_call(callee)
      arguments = []
      unless check(:RIGHT_PAREN)
        loop do
          if arguments.size >= 255
            error(peek(), "Can't have more than 255 arguments.")
          end
          arguments << expression()
          break unless match(:COMMA)
        end
      end

      paren = consume(:RIGHT_PAREN, "Expect ')' after arguments.")

      return Expression::Call.new(callee: callee, paren: paren, arguments: arguments)
    end
  end
end